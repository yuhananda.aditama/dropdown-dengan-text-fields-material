package id.bootcamp.dropdowncustomclass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import id.bootcamp.dropdowncustomclass.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //dataDummy
        val categoryList = ArrayList<KategoriData>()
        for (i in 0..20){
            val cat = KategoriData(i,"Kategori $i")
            categoryList.add(cat)
        }

        //set default value (biasanya jika kita melakukan edit)
        binding.actvKategori.setText(categoryList[5].toString())

        val adapter = ArrayAdapter(this,R.layout.list_item,categoryList)
        binding.actvKategori.setAdapter(adapter)
        binding.actvKategori.setOnItemClickListener { adapterView, view, i, l ->
            //Mendapatkan item yang dipilih
            val selectedItem: KategoriData? = adapter.getItem(i)
        }


    }
}