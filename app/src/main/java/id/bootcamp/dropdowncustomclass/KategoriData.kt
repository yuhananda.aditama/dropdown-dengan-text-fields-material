package id.bootcamp.dropdowncustomclass

data class KategoriData(
    val id: Int = 0,
    val name: String = ""
){
    override fun toString(): String {
        return name
    }
}